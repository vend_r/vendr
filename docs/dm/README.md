# Dependency Management

### Introduction to dependencies

Let's say I want to provide a way for my application to write log messages. I can abstract a logging component behavior to the following typescript interface.

```js

interface Logger {
    /**
    * Appends a message to the log.
    */
    log(severity: string, message: string);
}
```

A possible implementation of this interface can then be the following simple console logger.

```js
    class ConsoleLogger implements Logger {
        public log(severity: string, message: string) {
            console.log(`${severity}: ${message}`);
        }
    }
```

Ok so we have an interface and an implementation. How do we actually use it from anywhere in our application? A naive solution would be to create a new instance of such a logger whenever we need it. Here is simple class that outputs some data to the log.

```js
class SomeTask {
  private logger: Logger = new ConsoleLogger();
    constructor() {
        this.logger.log('info', 'Starting to do some stuff');
        // do some stuff...
        this.logger.log('info', 'Finished doing my stuff!');
    }
}
```

While this will work, it seems like a waste, since we'll be creating a Logger object per task instance when all loggers do the exact same thing. And if in addition we need our logger to maintain some state, such as the current log level, we would have to make sure that all tasks write to the same Logger instance so that we can change the log level only in one place.

