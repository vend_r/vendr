## Server / Client starter app ##

This project is intended to serve as a starting point for developing our app. It provides an example client/server application for storing and retrieving notes (currently only stored in the server memory).
It integrates the following technologies: 
### Common to both server and client
* TypeScript (https://www.typescriptlang.org/) in the entire stack (server & client).
* Gulp (http://gulpjs.com/) as a build system for our entire environment. It takes care of all the tedious tasks of compiling TypeScript, sass and running the development server by simply running 'gulp' from the project root.

### Server Side
* NodeJS with an ExpressJS (https://expressjs.com/) server(developed in TypeScript)
* InversifyJS (http://inversify.io/) for dependency injection support and easy creation of RESt endpoints.

### Client Side
* AngularJS 2 (https://angular.io/) as our client side application framework (developed in TypeScript)
* SASS (http://sass-lang.com/) provides support for much more robust styling.

### Running the project

Currently only tested on OSX.

* Install latest version of NodeJS
* Install gulp globally using: 
> sudo npm install --global gulp gulp-cli
* Clone this repository in SourceTree
* open a terminal and navigate to the project directory
* run the following only once:
> npm install && typings install
* run the project:
> gulp
* Open a browser and navigate to http://localhost:5000

