import {enableProdMode} from '@angular/core';  
import {AppComponent} from './app/app.component';
import { bootstrap }    from '@angular/platform-browser-dynamic';

enableProdMode();  
bootstrap(AppComponent, []);  