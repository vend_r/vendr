import { Aggregate } from '../infra/es/aggregate';
import { AggregateRoot, Create, Handle, Update } from '../infra/es/decorators';
import { UserWithAccountCreated } from './user.events';
import { User } from '../../client/app/common/user';
import { SignUpUser } from '../../client/app/common/user.commands';

@AggregateRoot('user')
export class UserAR extends Aggregate {

    id: string;
    user: User;

    @Create(SignUpUser)
    signUpUser(command: SignUpUser) {
        this.applyChange(new UserWithAccountCreated(command.email, command.password), command.who);
    }

    @Handle(UserWithAccountCreated)
    handleUserCreated(event: UserWithAccountCreated) {
        this.id = event.metadata.aggregateId;
    }
    
}