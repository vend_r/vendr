var jwt = require('jsonwebtoken');

/**
 * A middleware function to determine if the user calling a request is logged in.
 * Checks for a valid JWT token in the request header.
 */
var isLoggedIn = (req, res, next) => {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {

    // verifies secret 
    jwt.verify(token, 'abcdefgh', function (err, decoded) {
      if (err) {
        return res.status(403).send({
          success: false,
          message: 'No token provided.'
        });
      } else {
        // if everything is good, save to request for use in other routes
        req.user = decoded;
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });

  }
}

export { isLoggedIn }