/// <reference path="../../types.d.ts" />

import { TYPES, provide, inject } from '../infra/ioc';
import { injectable } from 'inversify';
import { User, UserDetails } from '../../client/app/common/user';
import { StorageService } from '../infra/storage/storage.service';

/**
 * Handles note related operations against the storage.
 */
@provide(TYPES.UserService)
export class UserService {

    private static PATH: string = '/users';

    constructor(
        @inject(TYPES.StorageService) private storageService: StorageService
    ) {
    }

    createUser(user: User): void {
        this.storageService.appendToArray(UserService.PATH, user);
    }

    getAllUsers(): User[] {
        return this.storageService.retrieve(UserService.PATH);
    }

    deleteAllUsers(): void {
        return this.storageService.remove(UserService.PATH);
    }

    updateUser(user: User): void {
        let users: User[] = this.getAllUsers();
        let existingUser:User = users.find(u => u.username == user.username);
        existingUser.firstName = user.firstName;
        existingUser.lastName = user.lastName;
        this.storageService.store('/users/', users);
    }

    getByUsername(username: string): User {
        return this.getAllUsers().find(user => user.username == username);
    }
}