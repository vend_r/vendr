/// <reference path="../../types.d.ts" />

import { Controller, Get, Post, Put, Delete, TYPE } from 'inversify-express-utils';
import { TYPES, provideNamed, inject } from '../infra/ioc';
import { Request } from 'express';
import { User, UserDetails } from '../../client/app/common/user';
import { injectable } from 'inversify';
import { UserService } from './user.service';
import * as jwt from 'jsonwebtoken';
import { isLoggedIn } from './middleware';

@provideNamed(TYPE.Controller, TYPES.AuthController)
@Controller('/auth')
export class AuthController {

    constructor(
        @inject(TYPES.UserService) private userService: UserService
    ) {
    }

    @Get('/testusers')
    generateTestUsers(): string {
        this.userService.deleteAllUsers();
        this.userService.createUser({ username: 'danbaryak', password: '123456', firstName: 'Dan', lastName: 'Bar-Yaakov' });
        this.userService.createUser({ username: 'another', password: '123456' });
        this.userService.createUser({ username: 'third', password: '123456' });
        return 'ok';
    }

    @Put('/user', isLoggedIn) 
    updateUser(req: Request): User {
        let user: User = <User>req.body;
        this.userService.updateUser(user);
        return <User> req.body;
    }

    @Get('/users', isLoggedIn)
    getAllUserDetails(): UserDetails[] {
        return this.userService.getAllUsers().map(user => <UserDetails>{
            username: user.username, displayName: `${user.firstName} ${user.lastName}`
        });
    }

    @Get('/something', isLoggedIn)
    getSomething(req: Request): string {
        
        return 'here is something';
    }

    @Get('/me', isLoggedIn)
    getCurrentUser(request: Request): any {
        return this.userService.getByUsername(request['user'].username);
    }

    @Post('/login')
    login(request: Request): any {
        let { username, password } = request.body;

        let user: User = this.userService.getByUsername(username);
        if (!user) {
            return { success: false, message: 'Authentication failed. User not found.' };
        }

        if (user.password != password) {
            return { success: false, message: 'Authentication failed. Wrong password.' };
        }
        
        let token = jwt.sign(user, 'abcdefgh', {
            expiresIn: 10000 // expires in 24 hours
        });
        return {
            success: true,
            token: token
        };
    }

    @Get('/users')
    getAllUsers(): User[] {
        return this.userService.getAllUsers();
    }
}