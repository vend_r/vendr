import { ArrayView, View } from '../infra/es';
import { ViewEntity } from '../../client/app/common/view.entity';

export class UserCredentials implements ViewEntity {
    id: string;
    email: string;
    password: string;
}

@View({
    identifier: 'users',
    name: 'users',
    aggregateType: 'user',
    sync: false
})
export class CredentialsView extends ArrayView<UserCredentials> {


}