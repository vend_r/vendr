import { Event } from '../infra/es/journal';
import { User } from '../../client/app/common/user';

export class UserWithAccountCreated extends Event {
    email: string;
    password: string;
    constructor(email: string, password: string) {
        super();
        this.email = email;
        this.password = password;
    }
}
