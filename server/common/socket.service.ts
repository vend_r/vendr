import { injectable } from 'inversify';
import { TYPES, provideNamed, inject } from '../infra/ioc';
import { Projection } from '../infra/es/view';
import { ViewEntity, Subscription, ObjectSummary, ObjectAddition, ObjectUpdate, ObjectDeletion } from '../../client/app/common/view.entity';
import * as _ from 'lodash';

var uuid = require('uuid');

class ViewSubscription {
    id: string;
    socket: SocketIO.Socket;
    viewName: string;
    constructor() {
        this.id = uuid.v1();
    }
}

/**
 * Handles automatic view synchronization with the client side.
 */
@provideNamed(TYPES.SocketService, "")
export class SocketService {
    
    private io: SocketIO.Server;
    private views: {[key: string] : Projection<any>} = {};
    private subscriptions: { [key:string] : ViewSubscription } = {};
    private viewSubscriptions: { [key: string] : ViewSubscription[] } = {};

    init(io: SocketIO.Server): void {
        this.io = io;
        io.on('connection', (socket: SocketIO.Socket) => {
            console.log(`User connected: ${socket}`);
            socket.on('subscribe', (data, cb) => {
                let view: Projection<any> = this.views[data.viewName];
                if (view) {
                    socket.emit('objects', { view: data.viewName, data: view.getAll({})});
                    
                    let subscription: ViewSubscription = new ViewSubscription();
                    subscription.socket = socket;
                    subscription.viewName = data.viewName;
                    this.addSubscription(subscription);

                    let summary: ObjectSummary = {
                        subscriptionId: subscription.id,
                        viewName: subscription.viewName,
                        data: view.getAll(data.filter)
                    };

                    cb(summary);
                }
            });

            socket.on('unsubscribe', data => {
                this.removeSubscription(data.id);
            })
        });
        
    }

    private addSubscription(subscription: ViewSubscription) {
        this.subscriptions[subscription.id] = subscription;
        let subscriptionsPerView: ViewSubscription[] = this.viewSubscriptions[subscription.viewName];
        if (! subscriptionsPerView) {
            subscriptionsPerView = <ViewSubscription[]> [];
            this.viewSubscriptions[subscription.viewName] = subscriptionsPerView;
        }
        subscriptionsPerView.push(subscription);
    }

    private removeSubscription(id: string) {
        delete this.subscriptions[id];
        for (let viewName in this.viewSubscriptions) {
            let subscriptionsPerView: ViewSubscription[] = this.viewSubscriptions[viewName];
            if (subscriptionsPerView) {
                _.remove(subscriptionsPerView, sub => sub.id == id);
            }
        }
    }

    /**
     * Publishes a new view under the specified name. Client sockets can then subscribe to the view to receive
     * real time updates.
     */
    publishView<T extends ViewEntity>(name: string, view: Projection<T>) {
        this.views[name] = view;
    }

    objectAdded(viewName: string, object: ViewEntity) {
        let subscriptions: ViewSubscription[] = this.viewSubscriptions[viewName];
        if (subscriptions) {
            subscriptions.forEach(subscription => {
                let addEvent: ObjectAddition = {
                    subscriptionId: subscription.id,
                    viewName: viewName,
                    objectId: object.id,
                    data: object
                };
                subscription.socket.emit('addition', addEvent);
            })
        }
    }

    objectRemoved(viewName: string, id: string) {
        let subscriptions: ViewSubscription[] = this.viewSubscriptions[viewName];
        if (subscriptions) {
            subscriptions.forEach(subscription => {
                let deleteEvent: ObjectDeletion = {
                    viewName: viewName,
                    subscriptionId: subscription.id,
                    objectId: id
                };
                subscription.socket.emit('delete', deleteEvent);
            });
        }
    }

    objectUpdated(viewName: string, object: ViewEntity) {
        let subscriptions: ViewSubscription[] = this.viewSubscriptions[viewName];
        if (subscriptions) {
            subscriptions.forEach(subscription => {
                let updateEvent: ObjectUpdate = {
                    viewName: viewName,
                    subscriptionId: subscription.id,
                    objectId: object.id,
                    data: object
                }
                subscription.socket.emit('update', updateEvent);
            });   
        }
    }
}