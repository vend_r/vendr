/// <reference path="../../types.d.ts" />

import { Controller, Get, Post, Put, Delete, TYPE } from 'inversify-express-utils';
import { TYPES, provideNamed, inject } from '../infra/ioc';
import { Request } from 'express';
import { Note } from '../../client/app/common/notes/note';
import { NotesService } from './notes.service';
import { injectable } from 'inversify';
import { isLoggedIn } from '../auth/middleware';

/**
 * Handles note related requests from the client side. It's important to only keep HTTP request
 * related handling in the controller and delegate the actual implementation to a dedicated service
 * (in this case NotesService).
 */
@provideNamed(TYPE.Controller, TYPES.NotesController)
@Controller('/notes')
export class NotesController {

    constructor(
        @inject(TYPES.NotesService) private notesService: NotesService
    ) {
        
    }

    /**
     * Retrives all notes.
     */
    @Get('/', isLoggedIn)
    public getNotes(): Note[] {
        return this.notesService.getNotes();
    }

    /**
     * Adds a new note.
     */
    @Post('/', isLoggedIn)
    public newNote(request: Request): Promise<Note> {
        var self = this;
        var note: Note = <Note>request.body;
        return this.notesService.addNote(note);
    }

    @Get('/clear', isLoggedIn)
    public clearNotes(request: Request): string {
        this.notesService.deleteAll();
        return 'ok';

    }

}