import { MaterializedView, ArrayView } from '../infra/es/view'
import { View, ClientSync, HandleGlobal, HandleCreateEvent, HandleUpdateEvent } from '../infra/es/decorators'
import { Event } from '../infra/es/journal';
import { NoteCreated, NoteNameChanged } from './note.ar';
import { Controller, Get, Post, Put, Delete, TYPE } from 'inversify-express-utils';
import { TYPES, provideNamed, inject } from '../infra/ioc';
import { Note } from '../../client/app/common/notes/note';
import { ShellCommand } from 'dependency-manager';

@View({
    identifier: 'notes',
    aggregateType: 'note',
    name: 'notes',
    sync: true
})
export class NoteView extends ArrayView<Note> {
    
    init(): void {
        this.info('Notes view is ready');
    }

    @HandleCreateEvent(NoteCreated)
    noteCreated(event: NoteCreated): Note {
        let note: Note = new Note(event.name, '', event.metadata.who);
        note.id = event.metadata.aggregateId;
        this.debug(`Note created: ${note.name} by user ${event.metadata.who}`);
        return note;
    }
    
    @HandleUpdateEvent(NoteNameChanged)
    noteNameChanged(note: Note, event: NoteNameChanged) {
        this.debug(`Note name changed from ${note.name} to ${event.name} by user ${event.metadata.who}`);
        note.name = event.name;
    }
    
    @ShellCommand({ name: 'notes show', description: 'Show all notes'})
    showNotes(req, res): void {
        res.cyan(JSON.stringify(this.data, null, 2)).ln();
    }

    @ShellCommand({ name: 'notes count', description: 'Show note count'})
    countNotes(req, res): void {
        res.cyan(this.data.length).ln();
    }
}

