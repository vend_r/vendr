import { Aggregate } from '../infra/es/aggregate';
import { AggregateRoot, Create, Handle, Update } from '../infra/es/decorators';
import { CreateNote, ChangeNoteName } from '../../client/app/common/notes/commands';
import { Event } from '../infra/es/journal';

export class NoteCreated extends Event {
    name: string;
    constructor(name: string) {
        super();
        this.name = name;
    }
}

export class NoteNameChanged extends Event {
    name: string;
    constructor(name: string) {
        super();
        this.name = name;
    }
}

@AggregateRoot('note')
export class NoteAR extends Aggregate {

    name: string;
    isEdited: boolean;

    @Create(CreateNote)
    createNote(command: CreateNote) {
        this.checkNotEmpty(command.name, 'Name cannot be empty');
        this.applyChange(new NoteCreated(command.name), command.who);
    }

    @Update(ChangeNoteName)
    changeName(command: ChangeNoteName) {
        this.checkNotEmpty(command.name, 'Name cannot be empty');
        this.applyChange(new NoteNameChanged(command.name), command.who);
    }

    @Handle(NoteCreated)
    noteCreated(event: NoteCreated) {
        this.name = event.name;
    }

    @Handle(NoteNameChanged)
    nameChanged(event: NoteNameChanged) {
        this.name = event.name;
    }
    
}