/// <reference path="../../types.d.ts" />

import { provide, inject } from '../infra/ioc/ioc';
import { Note } from '../../client/app/common/notes/note';
import { injectable } from 'inversify';
import { TYPES } from '../infra/ioc/constants';
import { StorageService } from '../infra/storage/storage.service';

/**
 * Handles note related operations against the storage.
 */
@provide(TYPES.NotesService)
export class NotesService {

    constructor(
        @inject(TYPES.StorageService) private storageService: StorageService
    ) {
        console.log(`NotesService created`);
    }

    public getNotes(): Note[] {
        return this.storageService.retrieve('/notes');
    }

    public addNote(note: Note): Promise<Note> {
        this.storageService.appendToArray('/notes', note);
        return new Promise<Note>((resolve, reject) => resolve(note));
    }

    public deleteAll(): void {
        this.storageService.remove('/notes');
    }

}