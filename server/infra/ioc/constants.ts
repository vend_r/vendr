/// <reference path="../../../types.d.ts" />

const TYPES = {
    StorageService: 'StorageService',
    SocketService: 'SocketService',
    NotesService: 'NotesService',
    NotesController: 'NotesController',
    UserService: 'UserService',
    AuthController: 'AuthController',
    CommandController: 'CommandController',
    Journal: 'Journal',
    MongoClient: 'MontoClient',
    PersonView: 'PersonView',
    PersonCounterView: 'PersonCounterView',
    NoteView: 'NoteView',
    View: 'View',
    ViewRegistry: 'ViewRegistry'
}

export { TYPES };