import '../../infra/storage/mongodb/client';
import '../es';
import '../../common/socket.service';
import '../../notes/notes.controller';
import '../../notes/notes.service';
import '../../auth/user.service';
import '../../auth/credentials.view';
import '../../auth/auth.controller';
import '../es/command.controller';
import '../../infra/storage/file/file.storage.service';
import '../../notes/note.view';
import '../../notes/note.ar';
import '../../auth/user.ar';