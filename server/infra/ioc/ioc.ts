import 'reflect-metadata';
import { Kernel, inject } from 'inversify';
import { autoProvide, makeProvideDecorator, makeFluentProvideDecorator } from 'inversify-binding-decorators';
import { makeLoggerMiddleware } from 'inversify-logger-middleware';
import { TYPES } from './constants';
import { TYPE } from 'inversify-express-utils';

let kernel = new Kernel();

if (process.env.NODE_ENV === 'development') {
    let logger = makeLoggerMiddleware();
    kernel.applyMiddleware(logger);
}

let provideOrig = makeProvideDecorator(kernel);

// let provideSingleton = function(identifier) {
//     return provideOrig(identifier)
//               .inSingletonScope()
//               .done();
// };

let provide = function (identifier) {
    return fluentProvider(identifier)
        .inSingletonScope()
        .whenTargetNamed("")
        .done();
};

let fluentProvider = makeFluentProvideDecorator(kernel);

let provideNamed = function (identifier, name) {
    return fluentProvider(identifier)
        .inSingletonScope()
        .whenTargetNamed(name)
        .done();
};


export { kernel, autoProvide, provide, provideNamed, inject, fluentProvider };
