import { Projection, ViewConfiguration } from '.';
import { provideNamed, TYPES } from '../ioc';

@provideNamed(TYPES.ViewRegistry, "")
export class ViewRegistry {

    views: { [key: string] : Projection<any> } = {};

    public registerView(view: Projection<any>, config: ViewConfiguration) {
        this.views[config.identifier] = view;
    }
}