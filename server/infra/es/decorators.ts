import { AggregateMetadata, CreateMetadata, UpdateMetadata, HandleMetadata} from './interfaces';
import { METADATA_KEY } from './constants';
import { kernel, TYPES, fluentProvider } from '../ioc';
import { CommandController } from './command.controller';
import { Controller, Get, Post, Put, Delete, TYPE } from 'inversify-express-utils';
import { AggregateDescriptor } from './aggregate.descriptor';
import { ViewRegistry } from './view.registry';
import { Projection } from '.';
import { DependencyManager } from 'dependency-manager';

require('reflect-metadata');

/**
 * 
 */
export function AggregateRoot(aggregateTypeId: string) {
    return function(target: any) {
        let metadata: AggregateMetadata = { aggregateTypeId: aggregateTypeId };
        Reflect.defineMetadata(METADATA_KEY.aggregateRoot, metadata, target);
        let descriptor: AggregateDescriptor = new AggregateDescriptor(aggregateTypeId, target);
        descriptor.createMetadata = Reflect.getOwnMetadata(METADATA_KEY.create, target);
        descriptor.handleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handle, target);
        descriptor.updateMetadata = Reflect.getOwnMetadata(METADATA_KEY.update, target);
        let commandController: CommandController = <CommandController> kernel.getNamed(TYPE.Controller, TYPES.CommandController);
        commandController.registerAggregate(descriptor);  
    }   
}

export function Create(command: any) {
    return function(target: any, key: string, value: any) {
        let metadata: CreateMetadata = { command: command.name, method: key };
        Reflect.defineMetadata(METADATA_KEY.create, metadata, target.constructor);
    }
}

export function Update(command: any) {
    return function(target: any, key: string, value: any) {
        let metadata:UpdateMetadata = Reflect.getOwnMetadata(METADATA_KEY.update, target.constructor);
        if (! metadata) {
            metadata = {
                methods: {}
            }
        }
        metadata.methods[command.name] = {
            method: key,
            commandType: command.name
        };
        Reflect.defineMetadata(METADATA_KEY.update, metadata, target.constructor);
    }
}

export function HandleGlobal(aggregateType: string, eventType: any) {
    return function(target: any, key: string, value: any) {
        let metadata:HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleGlobal, target.constructor);
        if (! metadata) {
            metadata = {
                methods: {}
            }
        }
        metadata.methods[eventType.name] = {
            method: key,
            eventType: eventType.name,
            aggregateType: aggregateType
        };
        Reflect.defineMetadata(METADATA_KEY.handleGlobal, metadata, target.constructor);
    }
}

export function Handle(eventType: any) {
    return function(target: any, key: string, value: any) {
        let metadata:HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handle, target.constructor);
        if (! metadata) {
            metadata = {
                methods: {}
            }
        }
        metadata.methods[eventType.name] = {
            method: key,
            eventType: eventType.name
        };
        Reflect.defineMetadata(METADATA_KEY.handle, metadata, target.constructor);
    }
}

export function HandleCreateEvent(eventType: any) {
    return function(target: any, key: string, value: any) {
        let metadata:HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleCreateEvent, target.constructor);
        if (! metadata) {
            metadata = {
                methods: {}
            }
        }
        metadata.methods[eventType.name] = {
            method: key,
            eventType: eventType.name
        };
        Reflect.defineMetadata(METADATA_KEY.handleCreateEvent, metadata, target.constructor);
    }
}

export function HandleUpdateEvent(eventType: any) {
    return function(target: any, key: string, value: any) {
        let metadata:HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleUpdateEvent, target.constructor);
        if (! metadata) {
            metadata = {
                methods: {}
            }
        }
        metadata.methods[eventType.name] = {
            method: key,
            eventType: eventType.name
        };
        Reflect.defineMetadata(METADATA_KEY.handleUpdateEvent, metadata, target.constructor);
    }
}

export function ClientSync() {
    return function(target: any) {
        
    }
}

export interface ViewConfiguration {
    identifier: string;
    
    aggregateType: string;

    name: string;

    sync: boolean;
}

export function View(config: ViewConfiguration) {
    return (target: any) => {
        let viewRegistry: ViewRegistry = <ViewRegistry> kernel.getNamed(TYPES.ViewRegistry, "");
        let view: any = Object.create(target.prototype);
        view.constructor.apply(view, [ config ]);
        DependencyManager.instance.registerServiceInstance(target.prototype, view, {});
        viewRegistry.registerView(view, config);
    }
};