import { Event, Journal } from './journal';
import { provide, inject } from '../ioc';
import { injectable } from 'inversify';
import { TYPES } from '../ioc';
import { AggregateDescriptor } from './aggregate.descriptor';
import { HandlerMethod } from './interfaces';

export class Aggregate {

    public id: string;
    public version: number;
    public type: string;
    public descriptor: AggregateDescriptor;

    constructor(private journal: Journal) {
        
    }

    applyChange(event: Event, who: string = ''): void {
        this.journal.emit(this.type, this.id, event, who);
        let method: HandlerMethod = this.descriptor.handleMetadata.methods[(<any>event).constructor.name];
        this[method.method](event);
    }

    checkNotEmpty(param: any, message: string) {
        if (! param) {
            throw message;
        }
    }

}