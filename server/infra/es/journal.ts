import { injectable } from 'inversify';
import { TYPES, provideNamed, inject } from '../ioc';
import { StorageService } from '../storage/storage.service';
import { MongoDBClient } from '../storage/mongodb/client';

export interface EventObserver {
    handleEvent(event: Event);
}

export interface EventMetadata {
    aggregateType: string;
    aggregateId: string;
    eventType: string;
    when: number;
    who: string;
}

export class Event {
    metadata: EventMetadata;
}

export interface Journal {
    emit(aggregateType: string, aggregateId: string, event: Event, who: string): void;
    replay(aggregateType: string, aggregateId: string, callback: (event: Event) => void, after: () => void );
    replayAll(aggregateType: string, callback: (event: Event) => void);
    addEventObserver(observer: EventObserver);
}

@provideNamed(TYPES.Journal, "Mongo")
export class MongoJournal implements Journal {

    private observers: EventObserver[] = [];

    constructor(
        @inject(TYPES.MongoClient) private mongoClient: MongoDBClient
    ) {
                
    }

    public addEventObserver(observer: EventObserver) {
        this.observers.push(observer);
    }

    /**
     * Stores an event in the journal.
     */
    public emit(aggregateType: string, aggregateId: string, event: any, who: string): void {
        event.metadata = {
            aggregateType: aggregateType,
            aggregateId: aggregateId,
            when: new Date().getTime(),
            eventType: event.constructor.name,
            who: who
        }
        this.mongoClient.insert(aggregateType, event, (error, data) => {
            this.observers.forEach(observer => observer.handleEvent(event));
        });
    }

    public replay(aggregateType: string, aggregateId: string, callback: (event: Event) => void, after: () => void ) {
        this.mongoClient.find(aggregateType, { "metadata.aggregateId": aggregateId }, (error, data) => {
            let events: Event[] = data;
            events.forEach(event => {
                callback(event);
            });
            after();
        })
    }

    /**
     * Replays events for all aggregates of the specified type.
     */
    public replayAll(aggregateType: string, callback: (event: Event) => void) {
        this.mongoClient.find(aggregateType, {}, (error, data) => {
            let events: Event[] = data;
            events.forEach(event => {
                callback(event);
            });
        });
    }

}