import { injectable } from 'inversify';
import { ViewConfiguration, AggregateMetadata, CreateMetadata
    , UpdateMetadata, HandleMetadata, HandlerMethod} from '.';
import { METADATA_KEY } from './constants';
import { EventObserver, Event, Journal } from './journal';
import { TYPES, kernel } from '../ioc';
import { SocketService } from '../../common/socket.service';
import { ViewEntity } from '../../../client/app/common/view.entity';
import { BaseService } from 'dependency-manager';

import * as _ from 'lodash';

@injectable()
export class MaterializedView implements EventObserver {

    constructor() {
        let handleMetadata: HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleGlobal, this.constructor);
        let journal: Journal = <Journal>kernel.getAll(TYPES.Journal)[0];
        journal.addEventObserver(this);
        let aggregateTypes: string[] = _(handleMetadata.methods)
            .map((method: HandlerMethod) => method.aggregateType)
            .union().value();
        setTimeout(() => {
            for (var type of aggregateTypes) {
                journal.replayAll(type, event => {
                    this.handleEvent(event);
                });
            }
        }, 5000)
    }

    handleEvent(event: Event): void {
        let handleMetadata: HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleGlobal, this.constructor);
        for (var method in handleMetadata.methods) {
            if (method === event.metadata.eventType) {
                let functionName: string = handleMetadata.methods[method].method;
                this[functionName](event);
            }
        }
    }

}

export interface Projection<T extends ViewEntity> {
    getAll(filter: any): T[];
    accept(object: T, filter: any): boolean;
}

@injectable()
export abstract class ArrayView<T extends ViewEntity> extends BaseService implements Projection<T>, EventObserver {

    protected data: T[];
    private socketService: SocketService;
    private config: ViewConfiguration;

    constructor(config: ViewConfiguration) {
        super();
        this.config = config;
        this.data = [];
        let metadata: HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleCreateEvent, this.constructor);
        this.socketService = <SocketService>kernel.getAll(TYPES.SocketService)[0];
        this.socketService.publishView(config.aggregateType, this);
        let journal: Journal = <Journal>kernel.getAll(TYPES.Journal)[0];
        journal.addEventObserver(this);
        setTimeout(() => {
            journal.replayAll(config.aggregateType, event => {
                this.handleEvent(event);
            });
        }, 5000)
    }
    
    public handleEvent(event: Event): void {
        if (this.config.aggregateType != event.metadata.aggregateType) {
            return;
        }
        let handleCreateMetadata: HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleCreateEvent, this.constructor);
        let aggregateId: string = event.metadata.aggregateId;
        if (event.metadata.eventType in handleCreateMetadata.methods) {
            let methodName: string = handleCreateMetadata.methods[event.metadata.eventType].method;
            let createdObject: T = this[methodName](event);
            this.data.push(createdObject);
            if (this.config.sync) {
                this.socketService.objectAdded(this.config.aggregateType, createdObject);
            }
            return;
        }

        let handleUpdateMetadata: HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleUpdateEvent, this.constructor);
        if (handleUpdateMetadata && (event.metadata.eventType in handleUpdateMetadata.methods)) {
            let methodName: string = handleUpdateMetadata.methods[event.metadata.eventType].method;
            let object: T = this.data.find(object => object.id == event.metadata.aggregateId);
            if (object) {
                this[methodName](object, event);
                if (this.config.sync) {
                    this.socketService.objectUpdated(this.config.aggregateType, object);
                }
            }
        }

        // TODO: handle deletion
        
        // let handleDeleteMeadata: HandleMetadata = Reflect.getOwnMetadata(METADATA_KEY.handleDeleteEvent, this.constructor);
        // if (event.metadata.eventType in handleDeleteMeadata.methods) {
        //     let methodName: string = handleDeleteMeadata.methods[event.metadata.eventType].method;
        //     let object: T = this.data.find(object => object.id == event.metadata.aggregateId);
        //     if (this[methodName](object, event)) {
        //         _.remove(this.data, obj => obj.id == event.metadata.aggregateId);
        //     }
        // }
    }

    getAll(filter: any): T[] {
        return this.data;
    }

    accept(object: T, filter: any): boolean {
        return true;
    }

    init(): void {
        
    }
}