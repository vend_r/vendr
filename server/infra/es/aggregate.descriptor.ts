import { CreateMetadata, UpdateMetadata, HandleMetadata } from './interfaces';

export class AggregateDescriptor {

    typeId: string;
    prototype: any;
    createMetadata: CreateMetadata;
    updateMetadata: UpdateMetadata;
    handleMetadata: HandleMetadata;

    constructor(typeId: string, prototype: any) {
        this.typeId = typeId;
        this.prototype = prototype;
    }

}
 