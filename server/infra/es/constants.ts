export const METADATA_KEY = {
    aggregateRoot: '_aggregate-root',
    create: '_aggregate-create',
    update: '_aggregate-update',
    handle: '_aggregate-handle',
    handleGlobal: '_handle-global',
    handleCreateEvent: '_handle-create-event',
    handleUpdateEvent: '_handle-update-event',
    handleDeleteEvent: '_handle-delete-event'
}