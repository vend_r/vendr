import { Controller, Get, Post, Put, Delete, TYPE } from 'inversify-express-utils';
import { TYPES, provideNamed, inject } from '../ioc';

import { injectable } from 'inversify';
import { Request } from 'express';
import { AggregateDescriptor } from './aggregate.descriptor';
import { Journal } from './journal';
import { Aggregate } from './aggregate';
import { Event } from './journal';
import { HandlerMethod, UpdateMethod } from './interfaces';
import { isLoggedIn } from '../../auth/middleware';
import { Command } from '../../../client/app/common/commands';
import { DependencyManager } from 'dependency-manager';

var uuid = require('uuid');

var EventStore = require('event-store-client');

@provideNamed(TYPE.Controller, TYPES.CommandController)
@Controller('/commands')
export class CommandController {

    constructor(
        @inject(TYPES.Journal) private journal: Journal
    ) {
        
    }

    aggregates: { [key: string]: AggregateDescriptor } = {};
    
    @Post('/:aggregate_type/:command_name', isLoggedIn)
    public handleCreateCommand(request: Request): any {
        let command: Command = request.body;
        let { aggregate_type, command_name } = request.params;
        let descriptor: AggregateDescriptor = this.aggregates[aggregate_type];
        command.who = request['user'].username;
        if (descriptor) {
            // create and initialize new aggregate root
            var instance: Aggregate = Object.create(descriptor.prototype.prototype);
            instance.descriptor = descriptor;
            instance.type = descriptor.typeId;
            instance.id = uuid.v1();
            instance.constructor.apply(instance, [this.journal]);
            instance[descriptor.createMetadata.method](command);
        }
        
        return { id: instance.id };
    }

    @Put('/:aggregate_type/:aggregate_id/:command_name', isLoggedIn)
    public handleUpdateCommand(request: Request): any {
        let command: Command = request.body;
        let { aggregate_type, aggregate_id, command_name } = request.params;
        let descriptor: AggregateDescriptor = this.aggregates[aggregate_type];
        command.who = request['user'].username;
        if (descriptor) {
            var instance: Aggregate = Object.create(descriptor.prototype.prototype);
            instance.descriptor = descriptor;
            instance.type = descriptor.typeId;
            instance.id = aggregate_id;
            instance.constructor.apply(instance, [this.journal]);

            // replay previous events
            this.journal.replay(aggregate_type, aggregate_id, (event: Event) => {
                 let method: HandlerMethod = descriptor.handleMetadata.methods[event.metadata.eventType];
                 instance[method.method](event);
            }, () => {
                // handle command
                let updateMethod: UpdateMethod = descriptor.updateMetadata.methods[command_name];
                instance[updateMethod.method](command);
            });
            
        }
        return { result: 'ok' };
    }

    public registerAggregate(descriptor: AggregateDescriptor) {
        this.aggregates[descriptor.typeId] = descriptor;
        var instance = Object.create(descriptor.prototype.prototype);
        instance.constructor.apply(instance);
        DependencyManager.instance.registerServiceInstance(descriptor.prototype, instance, {});
    }
}