export interface AggregateMetadata {
    aggregateTypeId: string 
}

export interface CreateMetadata {
    command: any;
    method: string;
}

export interface UpdateMetadata {
    methods: { [key: string]: UpdateMethod };
}

export interface UpdateMethod {
    method: string;
    commandType: string;
}

export interface HandleMetadata {
    methods: { [key: string]: HandlerMethod };
}

export interface HandlerMethod {
    method: string;
    eventType: string;
    aggregateType?: string;
}