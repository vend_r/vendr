/// <reference path="../../../../types.d.ts" />

var JsonDB = require('node-json-db');
import { provide, inject } from '../../ioc';
import { injectable } from 'inversify';
import { TYPES } from '../../ioc';
import { StorageService } from '../storage.service'
var JsonDB = require('node-json-db');

@provide(TYPES.StorageService)
export class FileStorageService implements StorageService {

    db: any;

    constructor() {
        this.db = new JsonDB("data", true, false);
    }

    store(path: string, object: any): void {
        this.db.push(path, object);
    }

    retrieve(path: string): any[] {
        try {
            return this.db.getData(path);
        } catch (error) {
            return [];
        }
    }

    appendToArray(path: string, object: any): void {
        this.db.push(path + '[]', object);
    }

    remove(path: string): void {
        this.db.delete(path);
    }
}