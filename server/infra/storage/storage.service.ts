/**
 * Abstraction for a document storage service.
 */
export interface StorageService {

    /**
     * Stores an object in the specified path. overwrites any existing content.
     */
    store(path: string, object: any): void;

    /**
     * Appends the specified object to an array in the specified path. Creates a new 
     * array if the path is empty.
     */
    appendToArray(path: string, object: any): void;

    /**
     * Retrieves the object in the specified path.
     */
    retrieve(path: string): any;

    /**
     * Removes any object in the specified path.
     */
    remove(path: string): void;
}