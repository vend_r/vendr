import { Db, MongoClient } from 'mongodb';

const url: string = process.env['MONGODB_URI'] || 'mongodb://localhost:27017/starter-project';

export class MongoDBConnection {
  private static isConnected: boolean = false;
  private static db: Db;

  public static getConnection(result: (connection) => void) {
    
    if (this.isConnected) {
      return result(this.db);
    } else {
      this.connect((error, db: Db) => {
        if (error) {
          console.log('error while connecting: ', error);
        }
        return result(this.db);
      });
    }
  }

  private static connect(result: (error, db: Db) => void) {
    console.log('connecting  ', url);
    MongoClient.connect(url, (error, db) => {
      console.log('connected');
      this.db = db;
      this.isConnected = true;

      return result(error, db);
    });
  }
}
