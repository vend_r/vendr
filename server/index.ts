/// <reference path="../types.d.ts" />

import * as express from 'express'
import * as morgan from 'morgan';
import * as jwt from 'jsonwebtoken';
import { InversifyExpressServer } from 'inversify-express-utils';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import { TYPES, kernel } from './infra/ioc'
import * as path from 'path';
import { SocketService } from './common/socket.service';
import { MyTest } from 'eventsourcing';
import 'dependency-manager';
import { DependencyManager } from 'dependency-manager';


import * as  SocketIO from 'socket.io';
var http = require('http');

// load all injectable entities.
// the @provide() annotation will then automatically register them.
import './infra/ioc/loader';

// start the server
let server = new InversifyExpressServer(kernel);

server.setConfig((app) => {
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(helmet());
  
});



var app = server.build();
let httpServer = http.createServer(app);

app.use(function(req: any, res, next){ //This must be set before app.router
   req.server = server;
   next();
});

let sio: SocketIO.Server = SocketIO();

sio.attach(httpServer);

let socketService: SocketService = <SocketService> kernel.getNamed(TYPES.SocketService, "");
socketService.init(sio);

// app.use(morgan('dev'));

app.use(express.static(__dirname + '/../client'));


app.all('*', (req: any, res: any) => {
  res.status(200).sendFile(path.join(__dirname, '../client/index.html'));
});
console.log('hellooooo!!!');
var port: number = process.env.PORT || 5000;


httpServer.listen(port);