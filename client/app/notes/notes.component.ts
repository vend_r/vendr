import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Note } from '../common/notes/note';
import { NotesService } from './notes.service';
import { User } from '../common/user';
import { UserService } from '../auth/user.service';
import { SocketService } from '../common/socket.service';
import { CommandService } from '../common/command.service';
import { CreateNote, ChangeNoteName } from '../common/notes/commands';

@Component({
    selector: 'notes',
    templateUrl: 'app/notes/notes.component.html',
    providers: [ NotesService ]
})
export class NotesComponent {

    title: string = 'This is the title';
    name: string = '';
    notes: Note[] = [];
    errorMessage: string;
    currentUser: User;
    editedNoteId: string;

    // @ViewChild('nameInput') nameInput: ElementRef;

    constructor(private notesService: NotesService, private userService: UserService
            , private socketService: SocketService, private commandService: CommandService, private renderer: Renderer) {
        this.userService.getActiveUser().subscribe(user => this.currentUser = user);
    }

    ngOnInit() {
        this.socketService.subscribe('notesView', 'note', {}, this.notes);
    }

    ngOnDestroy() {
        this.socketService.unsubscribe('notesView');
    }

    addNote(title: string) {
        if (!title) { return; }
        let command: CreateNote = new CreateNote(title);
        this.commandService.create('note', command);
    }

    deleteAllNotes(): void {
        this.notesService.deleteAllNotes();
        this.notes = [];
    }

    getUser(note: Note): string {
        if (note.userId) {
            return this.userService.getUserDisplayName(note.userId);
        }
        return '';
    }

    noteClicked(note: Note) {
        console.log(`Note ${note.id} was clicked`);
        this.editedNoteId = note.id;
    }

    changeNoteName(note: Note, name: string) {
        this.commandService.update('note', note.id, new ChangeNoteName(name));
        this.editedNoteId = undefined;
    }
}