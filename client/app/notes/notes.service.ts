import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Response } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Note } from '../common/notes/note'
import { HttpService } from '../http/http.service';

/**
 * Service for note related operations against the server.
 */
@Injectable()
export class NotesService {

    constructor(private httpService: HttpService) { }

    /**
     * Retrieves all notes from the server.
     */
    getNotes(): Observable<Note[]> {
        return this.httpService.get('notes')
            .map(body => {
                if (body.success == false) {
                    return <Note[]>[];
                }
                return body;
            });
    }

    /**
     * Addes a note.
     */
    addNote(note: Note): Observable<Note> {
        return this.httpService.post('notes', note)
            .catch(this.handleError);
    }

    deleteAllNotes(): void {
        this.httpService.get('notes/clear').subscribe();
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}