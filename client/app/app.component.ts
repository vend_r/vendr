import { Component } from '@angular/core';
import { NotesComponent } from './notes/notes.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './auth/user.service';
import { HttpService } from './http/http.service';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoggedInGuard } from './auth/logged.in.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html'
})
export class AppComponent {

  constructor(private userService: UserService, private router: Router) {
   
  
    userService.getActiveUser().subscribe(user => {
      console.log(`User is ${user}`);
    });    
  }

}