import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotesComponent } from '../notes/notes.component';
import { LoginComponent } from '../login/login.component';
import { UserService } from '../auth/user.service';
import { User } from '../common/user';
import { Observable } from 'rxjs/Rx';
import { LoggedInGuard } from '../auth/logged.in.guard';

@Component({
  selector: 'main',
  templateUrl: 'app/main/main.component.html',
  providers: [ LoggedInGuard ]

})
export class MainComponent {

  activeUserName: string;

  constructor(private userService: UserService, private router: Router) {
    this.userService.getActiveUser().subscribe(user => {
      if (user) {
        if (user.firstName) {
          this.activeUserName = `${user.firstName} ${user.lastName}`;
        } else {
          this.activeUserName = user.username;
        }
      } else {
        user = "";
      }
    })
  }
  
  logout() {
    this.userService.logout();
    this.router.navigate(['/login']);
  }

}