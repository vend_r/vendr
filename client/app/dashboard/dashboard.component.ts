import { Component, ViewChild } from '@angular/core';
import { UserService } from '../auth/user.service';
import { User } from '../common/user';
import { SocketService } from '../common/socket.service';
import { Subscription } from '../common/view.entity';
import { Note } from '../common/notes/note';

@Component({
    selector: 'dashboard',
    templateUrl: 'app/dashboard/dashboard.component.html'
})
export class DashboardComponent {
    // Pie
    
    public count: number;
    private subscriptionId: string;
    public notes: Note[] = [];
    public options: Object;

    constructor(private socketService: SocketService) {
        this.options = {
          chart: {
            type: 'column',
            margin: 75,
            options3d: {
      				enabled: true,
              alpha: 15,
              beta: 15,
              depth: 50
            }
          },
          plotOptions: {
            column: {
              depth: 25
            }
          },
          series: [{
            data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
          }]
        };
    }

    ngOnInit() {
        this.socketService.subscribe('dashboard', 'note', {}, this.notes);
    }
    ngOnDestroy() {
        this.socketService.unsubscribe('dashboard');
    }
    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }
    private datasets = [
        {
            label: "# of Votes",
            data: [12, 19, 3, 5, 2, 3]
        }
    ];

    private labels = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'];


    
}