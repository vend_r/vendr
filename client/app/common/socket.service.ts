import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { ObjectSummary, ObjectAddition, ObjectUpdate, ObjectDeletion } from './view.entity';

export class Subscription {
    id: string;
    collection: any[];
    componentId: string;
}

@Injectable()
export class SocketService {

    private socket;
    private subscriptions: { [key: string]: Subscription} = {};
    
    constructor() {
        this.socket = io();
        this.socket.on('addition', (addition: ObjectAddition) => {
            let subscription: Subscription = this.subscriptions[addition.subscriptionId];
            if (subscription) {
                subscription.collection.push(addition.data);
            }
        });

        this.socket.on('update', (update: ObjectUpdate) => {
            let subscription: Subscription = this.subscriptions[update.subscriptionId];
            if (subscription) {
                let modifiedObj: any = subscription.collection.find(obj => obj.id === update.objectId);
                if (modifiedObj) {
                    for (let k in update.data) {
                        modifiedObj[k] = update.data[k];
                    }                    
                }
            }
        });

        this.socket.on('delete', (removal: ObjectDeletion) => {
            let subscription: Subscription = this.subscriptions[removal.subscriptionId];
            if (subscription) {
                
            }
        });
    }

    on(message: string, callback): void {
        this.socket.on(message, callback);
    }

    emit(message: string, data: any) {
        this.socket.emit(message, data);
    }

    /**
     * Subscribes to a view.
     * 
     * @param componentId An identifier for this subscription.
     * @param viewName The view to subscribe to.
     * @param filter An optional subscription filter.
     * @param collection The collection to automatically update with the view data. 
     */
    public subscribe(componentId: string, viewName: string, filter: any, collection: any[]) {
        let subscriptionId: string;
        this.socket.emit('subscribe', { viewName: viewName, filter: filter }, (summary: ObjectSummary) => {
            let subscription: Subscription = new Subscription();
            subscription.id = summary.subscriptionId;
            subscription.componentId = componentId;
            subscription.collection = collection;
            this.subscriptions[summary.subscriptionId] = subscription;
            summary.data.forEach(object => {
                collection.push(object);
            });
            subscriptionId = summary.subscriptionId;
        });
        return subscriptionId;
    }

    /**
     * Unsubscribes from a view.
     * 
     * @param componentId The subscription identifier.
     */
    public unsubscribe(componentId: string) {
        for (let subscriptionId in this.subscriptions) {
            let subscription: Subscription = this.subscriptions[subscriptionId];
            if (subscription.componentId == componentId) {
                this.socket.emit('unsubscribe', { id: subscriptionId });
                delete this.subscriptions[subscriptionId];        
                return;
            }
        }
    }
}