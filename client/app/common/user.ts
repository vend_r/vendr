export interface User {
    username?: string,
    password?: string,
    firstName?: string,
    lastName?: string
}

export interface UserDetails {
    username: string,
    displayName: string
}