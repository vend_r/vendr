
export interface CreateNote {
    title: string;
    content: string;
}

export class Command {
    who: string;
}
