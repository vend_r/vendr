import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';

@Injectable()
export class CommandService {

    constructor(private httpService: HttpService) {

    }

    /**
     * Executes the given command on the server.
     */
    create(aggregateType: string, command: any) {
        this.httpService.post(`commands/${aggregateType}/${command.constructor.name}`, command).subscribe(result => {
            
        });
    }

    update(aggregateType: string, aggregateId: string, command: any) {
        this.httpService.put(`commands/${aggregateType}/${aggregateId}/${command.constructor.name}`, command).subscribe(result => {
            
        });
    }
}