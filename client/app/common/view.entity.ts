export interface ViewEntity {
    id: string;
}

export interface Subscription {
    view: string;
}

export interface ObjectSummary {
    viewName: string;
    subscriptionId: string;
    data: any[];
}

export interface ObjectAddition {
    viewName: string;
    subscriptionId: string;
    objectId: string;
    data: any;
}

export interface ObjectUpdate {
    viewName: string;
    subscriptionId: string;
    objectId: string;
    data: any;
}

export interface ObjectDeletion {
    viewName: string;
    subscriptionId: string;
    objectId: string;
}