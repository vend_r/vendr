import { ViewEntity } from '../view.entity';

export class Note implements ViewEntity {
    
    id: string;
    name: string;
    content: string;
    description: string = 'asdflkjaskldfja';
    userId: string;

    constructor(name: string, content: string, userId: string) {
        this.name = name;
        this.content = content;
        this.userId = userId;
    }
}