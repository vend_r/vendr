import { Command } from '../commands';

export class CreateNote extends Command {
    name: string;
    constructor(name: string) {
        super();
        this.name = name;
    }
}

export class ChangeNoteName extends Command {
    name: string;
    constructor(name: string) {
        super();
        this.name = name;
    }
}
