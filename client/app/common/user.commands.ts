import { Command } from './commands';

export class SignUpUser extends Command {
    email: string;
    password: string;
    constructor(email: string, password: string) {
        super();
        this.email = email;
        this.password = password;
    }
}