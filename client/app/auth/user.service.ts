import { Injectable } from '@angular/core';
import { Headers, Response, RequestOptions } from '@angular/http';
import { User, UserDetails } from '../common/user';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { Subject, BehaviorSubject, Observable } from 'rxjs/Rx';
import { HttpService } from '../http/http.service';

/**
 * Handles all authentication related operations against the server side.
 */
@Injectable()
export class UserService {

    private loggedIn: boolean;

    // A subject is a variable that can be observed from any part of the application.
    // When a new value is set to it, every observer gets the updated value.
    private activeUser: Subject<User> = new BehaviorSubject<User>(null);

    private allUsers: UserDetails[];

    constructor(private httpService: HttpService, private router: Router) {
        this.loggedIn = this.httpService.isTokenAvailable();
        if (this.loggedIn) {
            this.loadCurrentUser();
        }
    }

    /**
     * Sends a login request to the server. If all goes well receives an authentication token
     * that will be sent on any subsequent request from the app that requires authentication.
     */
    public login(username, password): any {
        let user: User = {
            username: username,
            password: password
        }
        return this.httpService.post('/auth/login', user)
            .map((res) => {
                if (res.success) {
                    localStorage.setItem('id_token', res.token);
                    this.loggedIn = true;
                    this.loadCurrentUser();
                }
                return res.success;
            });
    }


    public updateUserDetails(user: User): Observable<User> {
        return this.httpService.put('auth/user', user)
            .map(updatedUser => {
                this.loadCurrentUser();
                return updatedUser;
            });
    }

    /**
     * Loads the current user from the server side.
     */
    public loadCurrentUser(): void {
        this.httpService.get('auth/me').subscribe(result => {
            if (result) {
                this.activeUser.next(result);
                this.loadAllUsers();
            }
        }, error => {
            console.log('ERROR!');
            this.logout();
        });
    }

    private loadAllUsers(): void {
        this.httpService.get('auth/users')
            .subscribe(users => {
                this.allUsers = users;
                console.log(users);
            });
    }

    /**
     * Logs out of the system. Effectively this only clears the security token.
     */
    public logout(): void {
        this.httpService.removeToken();
        this.activeUser.next(null);
        this.loggedIn = false;
        this.router.navigate(['/login']);
    }

    /**
     * Returns true if a user is currently logged in.
     */
    public isLoggedIn() {
        return this.loggedIn;
    }

    public getActiveUser(): Observable<User> {
        return this.activeUser.asObservable();
    }

    public getUserDisplayName(username: string) {
        if (this.allUsers) {
            return this.allUsers.find(user => user.username == username).displayName;
        }
        return '';
    }
}