import { Component, ViewChild } from '@angular/core';
import { UserService } from '../auth/user.service';
import { User } from '../common/user';
@Component({
    selector: 'notes',
    templateUrl: 'app/settings/settings.html',
})
export class SettingsComponent {

    user: User = {};
    submitted: boolean = false;
    @ViewChild('profileForm') profileForm: any;

    constructor(private userService: UserService) {
        this.userService.getActiveUser().subscribe(user => {
            if (user != null) {
                this.user = user;
            }
        })
    }

    onSubmit() {
        this.submitted = true;
        this.userService.updateUserDetails(this.user).subscribe(user => {
            this.user = user;
        });
    }

    get diagnostic() { return JSON.stringify(this.user); }
}