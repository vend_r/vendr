import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { LoggedInGuard } from './auth/logged.in.guard';
import { UserService } from './auth/user.service';
import { HttpService } from './http/http.service';
import { SocketService } from './common/socket.service';
import { CommandService } from './common/command.service';
import { PopoverModule } from "ng2-popover";
import { enableProdMode } from '@angular/core';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from'./dashboard/dashboard.component';
import { InventoryComponent } from './inventory/inventory';
import { VendorsComponent } from './vendors/vendors';
import { SettingsComponent } from './settings/settings';
import { LoginComponent } from './login/login.component';
import { NotesComponent } from './notes/notes.component';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular2-highcharts';

@NgModule({
  providers: [
    LoggedInGuard,
    UserService,
    HttpService,
    SocketService,
    CommandService,
  ],
  imports: [
    ChartModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: MainComponent,
        canActivate: [LoggedInGuard],
        children: [
          { path: '', component: DashboardComponent, canActivate: [LoggedInGuard] },
          { path: 'inventory', component: InventoryComponent, canActivate: [LoggedInGuard] },
          { path: 'vendors', component: NotesComponent, canActivate: [LoggedInGuard] },          
          { path: 'settings', component: SettingsComponent, canActivate: [LoggedInGuard] }
        ]
      }, { path: 'login', component: LoginComponent }
    ])
  ],
  declarations: [
    AppComponent,
    MainComponent,
    SettingsComponent,
    DashboardComponent,
    LoginComponent,
    InventoryComponent,
    VendorsComponent,
    NotesComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}