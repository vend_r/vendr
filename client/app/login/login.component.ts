import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { UserService } from '../auth/user.service';

@Component({
    selector: 'login',
    templateUrl: 'app/login/login.component.html',    
})
export class LoginComponent {
    
    constructor(public router: Router, public http: Http, private userService: UserService) {
        
    }

    login(event, username, password) {
        this.userService.login(username, password).subscribe( result => {
            if (result) {
                this.router.navigate(['/']);
            }
        });
    }

    signup(event) {
        event.preventDefault();
        this.router.navigate(['/signup']);
    }
}