import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class HttpService {

    private static TOKEN_KEY: string = 'id_token';

    constructor(private http: Http) {
        
    }

    private getOptions() {
        let accessToken = localStorage.getItem(HttpService.TOKEN_KEY);
        if (accessToken) {
            let headers = new Headers({ 'x-access-token': localStorage.getItem(HttpService.TOKEN_KEY)});
            let options = new RequestOptions({ headers: headers });
            return options;
        }
        return {};
    }

    public isTokenAvailable(): boolean {
        return localStorage.getItem(HttpService.TOKEN_KEY) != undefined;
    }

    public removeToken(): void {
        localStorage.removeItem(HttpService.TOKEN_KEY);
    }
    
    /**
     * Sends a Get request to the server.
     */
    public get(path: string): Observable<any> {
        return this.http.get(path, this.getOptions()).map(res => res.json());
    }

    /**
     * Sends a Post request to the server.
     */
    public post(path: string, payload: any): Observable<any> {
        return this.http.post(path, payload, this.getOptions()).map(res => res.json());
    }

    /**
     * Sends a Put request to the server.
     */
    public put(path: string, payload: any): Observable<any> {
        return this.http.put(path, payload, this.getOptions()).map(res => res.json());
    }

    /**
     * Sends a Delete request to the server.
     */
    public delete(path: string): Observable<any> {
        return this.http.delete(path, this.getOptions()).map(res => res.json());
    }
}