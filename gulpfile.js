var gulp = require('gulp'),
  sassGlob = require('gulp-sass-glob'),
  nodemon = require('gulp-nodemon'),
  ts = require('gulp-typescript'),
  fs = require('fs'),
  livereload = require('gulp-livereload'),
  del = require('del'),
  sass = require('gulp-sass'),
  clientConfig = require('./client.config.json'),
  sourcemaps = require('gulp-sourcemaps');

gulp.task('typescript-server', function() {
  return tsCompile('server', true, 'es5');
});

gulp.task('typescript-client', function() {
  return tsCompile('client', false, 'es5');
});

function tsCompile(dir, absoluteSourceRoot, target, outdir) {
  if (! outdir) {
    outdir = dir;
  }
  var tsProject =  ts.createProject('tsconfig.json', { });
  var sourceRoot = ".";
  if (absoluteSourceRoot == true) {
    sourceRoot = __dirname + "/" + dir;
  }
  return gulp.src([dir + '/**/*.ts'])
    .pipe(sourcemaps.init())
    .pipe(ts({ 
        "target": target,
        "module": "commonjs",
        "moduleResolution": "node",
        "experimentalDecorators": true,
        "emitDecoratorMetadata": true,
         "types" : [
            "highcharts"
        ]
    })).pipe(sourcemaps.write('../' + outdir, { includeContent: absoluteSourceRoot == false , sourceRoot: sourceRoot })).pipe(gulp.dest('./deploy/' + outdir))
}

gulp.task('copy-client', function() {
  gulp.src(['client/**/*.js', 'client/**/*.html', 'client/**/*.svg', 'client/**/*.eot', 'client/**/*.ttf', 'client/**/*.woff', 'client/**/*.png', 'client/**/*.jpg'])
    .pipe(gulp.dest('./deploy/client'));

});

gulp.task('client-libs', function() {
  copyLibs(clientConfig.libs);
});

function copyLibs(libs) {
  for (var i in libs) {
    var libName = libs[i];
    gulp.src('./node_modules/' + libName + '/**/*').pipe(gulp.dest('./deploy/client/node_modules/' + libName));
  }
}

gulp.task('styles', function() {
    gulp.src('./client/**/*.scss')
        .pipe(sassGlob()).pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./deploy/client/'));
});

gulp.task('watch', function() {
  gulp.watch(__dirname + '/server/**/*.ts',  { interval: 500 }, ['typescript-server']);
  gulp.watch(__dirname + '/modules/**/*.ts',  { interval: 500 }, ['modules', 'typescript-server']);
  gulp.watch(__dirname + '/client/**/*.ts',  { interval: 500 }, ['typescript-client']);
  gulp.watch([__dirname + '/client/**/*.js', __dirname + '/client/**/*.html'],  { interval: 500 }, ['copy-client']);
  gulp.watch([__dirname + '/client/**/*.scss'],  { interval: 500 }, ['styles']);
});

gulp.task('serve', ['modules', 'typescript-server', 'typescript-client', 'copy-client', 'client-libs', 'styles', 'watch'], function () {
  nodemon({
    script: 'deploy/server/index.js',
    nodeArgs: ['--debug'],
    ext: 'js',
  });
});

gulp.task('modules', function() {
    return tsCompile('modules', true, 'es5', 'node_modules');
});

gulp.task('production', ['typescript-server', 'typescript-client', 'copy-client', 'client-libs', 'styles'], function() {

});

gulp.task('deploy', ['typescript'], function() {
  return gulp.src(['package.json'])
    .pipe(gulp.dest('./deploy'));
});


gulp.task('clean', function(cb) {
  del(['./deploy'], cb);
});

gulp.task('default', ['serve']);